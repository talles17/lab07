RM= rm -rf
CC=g++

LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test

CFLAGS= -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

.PHONY: all clean debug doc doxy

all: questao1 questao3

debug: CFLAGS += -g -O0
debug: questao1 questao3

questao1: CFLAGS += -I$(INC_DIR)/questao1

questao1: $(OBJ_DIR)/questao1/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao1 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'questao1' criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/questao1/main.o: $(SRC_DIR)/questao1/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


questao3: CFLAGS += -I$(INC_DIR)/questao3

questao3: $(OBJ_DIR)/questao3/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao3 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'questao3' criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/questao3/main.o: $(SRC_DIR)/questao3/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<



clean: 
	$(RM) $(DOC_DIR)/*
	$(RM) $(OBJ_DIR)/*