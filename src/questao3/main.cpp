#include <iostream>
using std::cout ;

#include <stack>
using std::stack ;


int main (int argc , char* argv[]) {

	stack<char> s ;
	for (int i = 0 ; i < argc-1 ; i++) {
		s.push(*argv[i]) ;
	}

	while (!s.empty()) {
		cout  << s.top() <<  " " ;
		s.pop() ;
	}

	return 0 ;


}