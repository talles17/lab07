#ifndef CLOSEST2MEAN_H
#define CLOSEST2MEAN_H

#include <vector>
using std::vector;

#include <numeric>

template<typename InputIterator>
InputIterator closest2mean(InputIterator first, InputIterator last) {
	int sum = std::accumulate (first , last , 0) ;

	return sum ;
}


#endif /* CLOSEST2MEAN_H */